library(palmerpenguins)
library(ggext)
library(tidyverse)
mydata<- palmerpenguins::penguins
t_Likert = function(df,var) { ##This function makes a frequency table for the Likert items
  df |>
    select({{var}}) |>
    drop_na() |>
    group_by({{var}}) |>
    summarise(Freq = n()) |>
    mutate(Pct = round(Freq/sum(Freq),3))
}
my_summary <- t_Likert(mydata, sex)
my_summary$item <- "Sex"
my_summary$lab <-  paste0(sprintf("%.0f", my_summary$Pct*100),"%")

ggplot(my_summary, aes(x=item)) +
  geom_col(aes(y=Pct, fill=sex),
           position = position_stack(reverse = TRUE),
           width = .5) +
  geom_text(aes(y = Pct,label=lab),
            position=position_stack(vjust=0.5), colour="black" ) +
  coord_flip() +
  scale_y_continuous(labels = percent_format()) +
  scale_fill_manual(values = c("#7FC97F", "#BEAED4")) +
  labs(y="", x="")+
  theme(legend.position = "bottom",
        legend.title = element_blank(),
        legend.text = element_text(),
        axis.ticks.y  = element_blank(),
        axis.text.y = element_markdown()) 