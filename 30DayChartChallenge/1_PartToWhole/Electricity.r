library(tidyverse)
library(patchwork)
########################################
# Purpose of program is to make a panel plot
# of the sources of electricity for North America
# in 2003-2020. The data come from an older
# Tidy Tuesday data set from 2022. This chart
# is for Day 1--Part to Whole.
#########################################

#Read in data set
technology <- readr::read_csv('https://raw.githubusercontent.com/rfordatascience/tidytuesday/master/data/2022/2022-07-19/technology.csv')
head(technology)
#labels <- technology %>% select(label) %>% distinct()

#Only use the data about electricity production
electricity <- technology %>% filter(startsWith(label, "Elec"))

#Recode the variables about electricty production
electricity$variable <- recode(electricity$variable, elec_coal = "Coal", elec_gas="Gas", elec_hydro="Hydroelectric", elec_nuc="Nuclear", elec_oil="Oil",
                               elec_renew_other="Other renewables", elec_solar="Solar", elec_wind="Wind")
#Delete observations that are about electricy generating capacity
electricity <- electricity |> filter(variable !='elec_cons')
electricity <- electricity |> filter(variable !='electric_gen_capacity')


p1 <- electricity %>% filter(year >= 2003 & iso3c =='USA') %>% 
  ggplot(aes(x=year, y=value, fill=variable)) +
  ylab("Terawatt Hours") +
  geom_area() +
  labs(x = '')+
  ggtitle("USA") +
  theme_minimal() +
  theme(legend.title=element_blank()) 
###################################################
p2 <- electricity %>% filter(year >= 2003 & iso3c =='CAN') %>% 
  ggplot(aes(x=year, y=value, fill=variable)) +
  ylab("Terawatt Hours") +
  geom_area() +
  labs(x = '')+
  ggtitle("Canada") +
  theme_minimal() +
  theme(legend.title=element_blank()) 
###################################################
p3 <- electricity %>% filter(year >= 2003 & iso3c =='MEX') %>% 
  ggplot(aes(x=year, y=value, fill=variable)) +
  ylab("Terawatt Hours") +
  geom_area() +
  ggtitle("Mexico") +
  labs(x = '')+
  theme_minimal() +
  theme(legend.title=element_blank()) 
p_all <- p1 + p2 + p3 + 
  plot_annotation(title='In 2003-2020, Canada produced more power from hydroelectricity',
                  subtitle = 'as a proportion of electricty produced.',
                  caption='@DataAngler@vis.social | Technology Adoption Dataset') +
  plot_layout(guides = 'collect') &
  theme(legend.position = 'bottom',
        legend.direction = 'horizontal',)
 p_all       
