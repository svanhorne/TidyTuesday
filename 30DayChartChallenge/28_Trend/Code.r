library(tidyverse)
library(ggthemes)
library(ggtext)

# Data set comes from public data file: https://opendata.firstmap.delaware.gov/datasets/delaware::delaware-public-crash-data-2-0/explore
d <- read_csv("./Data/Delaware_Public_Crash_Data_2.0.csv") 

d$CRASH_YEAR <- substr(d$CRASH_DATE, 1, 4)

d |> select(CRASH_YEAR, ALCOHOL_INVOLVED, CRASH_CLASS) |> 
  filter(CRASH_CLASS=='04' & CRASH_YEAR < 2022) |> 
  group_by(CRASH_YEAR) |> 
  summarise(n=n()) |> 
  ggplot(aes(x = CRASH_YEAR, y = n, group=1)) + 
  geom_line()+
  geom_point(stat = "identity", color="steelblue") +
  ylab("") +
  xlab("") +
  scale_y_continuous(limits = c(0,150)) +
  labs(title = "Fatal automobile crashes in Delaware have been increasing since 2018",
       caption="Plot by @DataAngler@vis.social | Data from Delaware Crash 2.0 Dataset")+
  theme_economist(base_size = 10) +
  theme(plot.title = element_textbox_simple()) 
ggsave("./Charts/Trend_FatalCrashes_DE.png", units = ("in"), width = 6, height = 4, dpi = 300)
